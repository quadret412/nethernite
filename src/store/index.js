import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// в рабочем проекте я бы использовал modules: {} //
// для атомарности store и для легкой поддержки кода, //
// так как логика обработки разных компонентов, и разных //
// взаимодействий с бэкендом была бы обособлена для всех необходимых компонентов //

// есть ошибка в консоли, которую можно было с легкостью избежать, //
// при наличии реального запроса на бэкед.constructorБыл бы создан //
// ассинхронный экшен для получения данных, на время обработки которого //
// устанавливался бы какой-нибудь флаг на подобии isLoading, который показывал //
// бы загрузчик, до момента получения и обработки ответа от сервера //

export default new Vuex.Store({
  state: {
    nowNews: {},
    relatedNews: [
      {
        id: 0,
        title: 'Welcome to Nethernite 1',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
      },
      {
        id: 1,
        title: 'Welcome to Nethernite 2',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
      },
      {
        id: 2,
        title: 'Welcome to Nethernite 3',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
      },
    ],
    currentNews: [
      {
        id: 0,
        imgSrc: './assets/img/Layer_1151.png',
        title: 'The International 2018 Collector’',
        date: 'July 21, 2018',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. '
      + 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took '
      + 'a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but '
      + 'also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s '
      + 'with the release of Letraset sheets containing Lorem Ipsum passages',
        secondDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
      + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
        likes: 123,
        views: 1111,
        comments: [
          {
            avatarSrc: '../assets/img/comment-avatar.png',
            name: 'Svetlakov 1',
            date: '21 May 2019',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
          + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
            likes: 3,
          },
        ],
      },
      {
        id: 1,
        imgSrc: './assets/img/Layer_1151.png',
        title: 'The International 2019 Collector’',
        date: 'July 22, 2019',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. '
      + 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took '
      + 'a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but '
      + 'also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s '
      + 'with the release of Letraset sheets containing Lorem Ipsum passages',
        secondDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
      + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
        likes: 456,
        views: 2222,
        comments: [
          {
            avatarSrc: '../assets/img/comment-avatar.png',
            name: 'Svetlakov 4',
            date: '21 May 2019',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
          + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
            likes: 3,
          },
          {
            avatarSrc: '../assets/img/comment-avatar.png',
            name: 'Svetlakov 5',
            date: '22 May 2019',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
          + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
            likes: 2,
          },
        ],
      },
      {
        id: 2,
        imgSrc: './assets/img/Layer_1151.png',
        title: 'The International 2020 Collector’',
        date: 'July 23, 2020',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. '
      + 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took '
      + 'a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but '
      + 'also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s '
      + 'with the release of Letraset sheets containing Lorem Ipsum passages',
        secondDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
      + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
        likes: 789,
        views: 3333,
        comments: [
          {
            avatarSrc: '../assets/img/comment-avatar.png',
            name: 'Svetlakov 7',
            date: '21 May 2019',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
          + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
            likes: 3,
          },
          {
            avatarSrc: '../assets/img/comment-avatar.png',
            name: 'Svetlakov 8',
            date: '22 May 2019',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
          + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
            likes: 2,
          },
          {
            avatarSrc: '../assets/img/comment-avatar.png',
            name: 'Svetlakov 9',
            date: '23 May 2019',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
          + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. ',
            likes: 1,
          },
        ],
      },
    ],
    gems: 142,
  },
  mutations: {
    createComment(state, newComment) {
      state.nowNews.comments.unshift(newComment);
    },
    updateLikeCounter(state, newLikesCount) {
      state.nowNews.likes = newLikesCount;
    },
    sortedComments(state, action) {
      if (action === 'likes') {
        state.nowNews.comments.sort((a, b) => ((a.likes < b.likes) ? 1 : -1));
      } if (action === 'date') {
        state.nowNews.comments.sort((a, b) => ((a.date < b.date) ? 1 : -1));
      }
    },
    setNowNews(state, id) {
      state.nowNews = {};
      state.nowNews = state.currentNews.find(el => el.id === id);
    },
  },
  actions: {
    fetchNews({ commit }, id = 1) {
      commit('setNowNews', id);
    },
  },
  getters: {
    getCurrentNews(state) {
      return state.nowNews;
    },
    getCurrentNewsComments(state) {
      return state.nowNews.comments;
    },
    getRelatedNews(state) {
      return state.relatedNews;
    },
    getGems(state) {
      return state.gems;
    },
  },
});
